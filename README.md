# Ansible Role NVM [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Role for installing NVM on Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git@gitlab.com:araulet-team/devops/ansible/ansible-role-nvm.git
  name: araulet.nvm
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.nvm
    become: true
    vars:
      nvm_install: true
      nvm_version: 0.34.0
    tags: [ 'nvm' ]
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet